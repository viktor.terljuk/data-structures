import org.junit.Test;
import static org.junit.Assert.assertTrue;
//dfdfdg

public class LinkedListTest {

    @Test
    public void testLinkedListAddAndGetMethods() {
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        assertTrue(1 == (int) linkedList.get(0));
        assertTrue(2 == (int) linkedList.get(1));
        assertTrue(3 == (int) linkedList.get(2));
    }
}

