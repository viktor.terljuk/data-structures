import org.junit.Test;
import static org.junit.Assert.assertTrue;
//sfffffffffff
public class ArrayListTest {

    @Test
    public void testArrayListAddAndGetMethods() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        assertTrue(1 == arrayList.get(0));
        assertTrue(2 == arrayList.get(1));
        assertTrue(3 == arrayList.get(2));
    }

    @Test
    public void testArrayListAddMethod() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(10,1);
        assertTrue(10 == arrayList.get(1));
    }

    @Test
    public void testArrayListDeleteMethod() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.remove(1);
        assertTrue(null == arrayList.get(1));
    }

    @Test
    public void testArrayListSetMethod() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.set(10, 1);
        assertTrue(10 == arrayList.get(1));
    }

    @Test
    public void testArrayListClearAndIsEmptyMethod() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.clear();
        assertTrue(arrayList.isEmpty() == true);
    }

    @Test
    public void testArrayListSizeMethod() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        assertTrue(3 == arrayList.size());
    }
}
