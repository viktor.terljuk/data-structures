import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
public class ArrayList<T> implements Iterable <T> {

    private int size = 0;
    private int capacity = 10;
    private Object[] arrayList;

    public ArrayList() {
        arrayList = new Object[capacity];
    }


    public void add(T value) {
        if (size == arrayList.length) {
            growCapacity();
        }

        arrayList[size++] = value;
    }

    private void growCapacity() {
        capacity *=2;
    }

    public void add(T value, int index) {
        if (size == arrayList.length) {
            growCapacity();
        }
        arrayList[index] =  value;
        size++;
    }

    public T remove(int index) {
        arrayList[index] = null;
        return null;
    }

    public T get(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) arrayList[index];
    }

    public T set(T value, int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        arrayList[index] = (T) value;
        return value;
    }

    public void clear() {
        size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        if (size > 0) {
            return false;
        }
        return true;
    }

    public boolean contains(T value) {
        for (Object object : arrayList) {
            if (object == value) {
                return true;
            }
        }
        return false;
    }

    public int indexOf(java.lang.Object value) {
        for (int i = 0; i <size ; i++) {
            if (arrayList[i] == value) {
                return i;
            }
        }
        return 0;
    }

    public int lastIndexOf(java.lang.Object value) {
        for (int i = 0; i <size ; i++) {
            if (arrayList[i] == value && arrayList[i+1] != value) {
                return i;
            }
        }
        return 0;
    }

    public Iterator<T> iterator() {
        return null;
    }

    public void forEach(Consumer<? super T> action) {

    }

    public Spliterator<T> spliterator() {
        return null;
    }
}
