//dgdgsgsdf
public class LinkedList<T> {
    private Node head;
    private int size;

    public LinkedList() {

    }

    public void add(T value) {
        Node oldHead = head;
        head = new Node(value);
        head.next = oldHead;
        size++;
    }

    public void add(int index, T value) {
        Node oldHead = head;
        Node temp;

        for (int i = 0; i < index - 1; i++) {
            oldHead = oldHead.next;
        }
        temp = oldHead.next;
        oldHead.next = new Node(value);
        oldHead.next.next = temp;
        size++;
    }

    public void remove(int index) {
        Node oldHead = head;

        for (int i = 0; i < index - 1; i++) {
            oldHead = oldHead.next;
        }
        oldHead.next = oldHead.next.next;
        size--;
    }

    public void clear() {
        Node oldHead = head;

        for (int i = 0; i < size; i++) {
            oldHead = null;
        }
        oldHead.next = null;
    }

    public Object get(int index) {

        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) {
            return head;
        }

        Node oldHead = head.next;

        for (int i = 0; i < index; i++) {

            if (oldHead.next == null) {
                return null;
            }
            oldHead = oldHead.next;
        }
        return oldHead;
    }

    public boolean contains(T value) {
        if (head == null) {
            return false;
        }

        if (head == value) {
            return true;
        }

        while (head.next != null) {
            head = head.next;

            if (head.next == value) {
                return true;
            }
        }
        return false;
    }

    boolean isEmpty() {
        return head == null;
    }

    private int size() {
        return size;
    }

}
